<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Employee
 * @package App\Models
 * @version March 6, 2022, 4:46 am UTC
 *
 * @property string $name
 * @property string $nik
 * @property string $alamat
 * @property string $jen_kel
 */
class Employee extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'employees';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'nik',
        'alamat',
        'jen_kel'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'nik' => 'string',
        'alamat' => 'string',
        'jen_kel' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'nullable|string|max:255',
        'nik' => 'nullable|string|max:16|unique:employees',
        'alamat' => 'nullable|string',
        'jen_kel' => 'nullable|string|max:13',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function contract()
    {
        return $this->hasOne(Contract::class);
    }


}
