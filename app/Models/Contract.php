<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Contract
 * @package App\Models
 * @version March 6, 2022, 6:16 am UTC
 *
 * @property \App\Models\Employee $employee
 * @property \App\Models\Position $position
 * @property integer $employee_id
 * @property integer $position_id
 * @property string $date_start
 * @property string $date_end
 */
class Contract extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'contracts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'employee_id',
        'position_id',
        'date_start',
        'date_end'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'employee_id' => 'integer',
        'position_id' => 'integer',
        'date_start' => 'string',
        'date_end' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'employee_id' => 'nullable|integer',
        'position_id' => 'nullable|integer',
        'date_start' => 'nullable',
        'date_end' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id');
    }
}
