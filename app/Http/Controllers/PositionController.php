<?php

namespace App\Http\Controllers;

use App\DataTables\PositionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePositionRequest;
use App\Http\Requests\UpdatePositionRequest;
use App\Repositories\PositionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PositionController extends AppBaseController
{
    /** @var  PositionRepository */
    private $positionRepository;

    public function __construct(PositionRepository $positionRepo)
    {
        $this->positionRepository = $positionRepo;
    }

    /**
     * Display a listing of the Position.
     *
     * @param PositionDataTable $positionDataTable
     * @return Response
     */
    public function index(PositionDataTable $positionDataTable)
    {
        return $positionDataTable->render('positions.index');
    }

    /**
     * Show the form for creating a new Position.
     *
     * @return Response
     */
    public function create()
    {
        return view('positions.create');
    }

    /**
     * Store a newly created Position in storage.
     *
     * @param CreatePositionRequest $request
     *
     * @return Response
     */
    public function store(CreatePositionRequest $request)
    {
        $input = $request->all();

        $position = $this->positionRepository->create($input);
        if($request->ajax()){
            return response()->json([
                'status' => 'success',
                'data' => $position
            ],200);

        }
        Flash::success(__('messages.saved', ['model' => __('models/positions.singular')]));

        return redirect(route('positions.index'));
    }

    /**
     * Display the specified Position.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $position = $this->positionRepository->find($id);

        if (empty($position)) {
            Flash::error(__('messages.not_found', ['model' => __('models/positions.singular')]));

            return redirect(route('positions.index'));
        }

        return view('positions.show')->with('position', $position);
    }

    /**
     * Show the form for editing the specified Position.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $position = $this->positionRepository->find($id);

        if (empty($position)) {
            Flash::error(__('messages.not_found', ['model' => __('models/positions.singular')]));

            return redirect(route('positions.index'));
        }

        return view('positions.edit')->with('position', $position);
    }

    /**
     * Update the specified Position in storage.
     *
     * @param  int              $id
     * @param UpdatePositionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePositionRequest $request)
    {
        $position = $this->positionRepository->find($id);

        if (empty($position)) {
            Flash::error(__('messages.not_found', ['model' => __('models/positions.singular')]));

            return redirect(route('positions.index'));
        }

        $position = $this->positionRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/positions.singular')]));

        return redirect(route('positions.index'));
    }

    /**
     * Remove the specified Position from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $position = $this->positionRepository->find($id);
        if (@$position->contract){
            Flash::error('Jabatan masih mempunyai kontrak aktif');

            return redirect(route('employees.index'));
        }
        if (empty($position)) {
            Flash::error(__('messages.not_found', ['model' => __('models/positions.singular')]));

            return redirect(route('positions.index'));
        }

        $this->positionRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/positions.singular')]));

        return redirect(route('positions.index'));
    }
}
