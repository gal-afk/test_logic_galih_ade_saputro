<?php

namespace App\Http\Controllers\Api;

use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponserTrait;



class AppController extends Controller
{

    use ApiResponserTrait;

    public function get_roles()
    {
        $data = \App\Models\Role::where('guard_name','api')->select('id','name')->get();

        if(!$data){
            return $this->error('Data not found',204);
        }

        return $this->success($data);
    }
}