<?php

namespace App\Http\Controllers\Api;

use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponserTrait;



class MemberController extends Controller
{
    use ApiResponserTrait;

    public function show()
    {
        $user = auth()->guard('api')->user();
        $user->load('roles');
        $user->makeHidden('password','created_at','updated_at','deleted_at');
        return $this->success( $user ,'Data received successfully');
    }
}
