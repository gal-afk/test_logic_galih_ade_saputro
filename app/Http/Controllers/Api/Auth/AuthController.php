<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\Member;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Hash;


class AuthController extends Controller
{

    use ApiResponserTrait;

    public function register(Request $request)
    {
        try {
            $attr = [
                'name'  => 'required|string|max:255',
                'email' => 'required|string|unique:users|email',
                'password' => 'required|string|min:6'
            ];

            $validator = Validator::make($request->all(), $attr);

            if($validator->fails()) {
                return $this->error($validator->messages(),200);
            }

            $user = User::create([
                'name' => $request->name,
                'password' => bcrypt($request->password),
                'email' => $request->email,
            ]);

            if($request->roles <> ''){
                $user->roles()->attach($request->roles);
            }

            $user->makeHidden('password','created_at','updated_at','deleted_at');

            return $this->success([],'Registration success');
        } catch (\Throwable $th) {
            return $this->error('Something wrong',204);
        }
    }

    public function login(Request $request)
    {
        $attr = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|min:6'
        ]);

        $user = User::where('email',$attr['email'])->first();
        config(['auth.guards.api.driver' => 'session']);
        if(Auth::guard('api')->attempt(['email' => $request->email, 'password' => $request->password])) {
            //generate the token for the user
            $user->load('roles');
            $user->makeHidden('password','created_at','updated_at','deleted_at');
            return $this->success([
                'user' => $user,
                'token' => auth()->guard('api')->user()->createToken('authToken')->accessToken
            ],'Data retrieved successfully');
        } else {
            return $this->error('Anauthorized',204);
        }

    }

    public function logout()
    {
        auth()->guard('api')->user()->token()->revoke();
        return response([
            'status' => true,
            'message' => 'Logged out'
        ], 200);

    }
}
