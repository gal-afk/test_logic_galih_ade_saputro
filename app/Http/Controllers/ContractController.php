<?php

namespace App\Http\Controllers;

use App\DataTables\ContractDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateContractRequest;
use App\Http\Requests\UpdateContractRequest;
use App\Repositories\ContractRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\PositionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ContractController extends AppBaseController
{
    /** @var  ContractRepository */
    private $contractRepository;

    public function __construct(ContractRepository $contractRepo)
    {
        $this->contractRepository = $contractRepo;
        $this->data = [
            'employee_list' => EmployeeRepository::getList(),
            'position_list' => PositionRepository::getList()
        ];
    }

    /**
     * Display a listing of the Contract.
     *
     * @param ContractDataTable $contractDataTable
     * @return Response
     */
    public function index(ContractDataTable $contractDataTable)
    {
        return $contractDataTable->render('contracts.index');
    }

    /**
     * Show the form for creating a new Contract.
     *
     * @return Response
     */
    public function create()
    {
        return view('contracts.create')->with($this->data);
    }

    /**
     * Store a newly created Contract in storage.
     *
     * @param CreateContractRequest $request
     *
     * @return Response
     */
    public function store(CreateContractRequest $request)
    {
        $input = $request->all();

        $contract = $this->contractRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/contracts.singular')]));
        if($request->ajax()){
            return response()->json([
                'status' => 'success',
                'data' => $contract
            ],200);

        }
        return redirect(route('contracts.index'));
    }

    /**
     * Display the specified Contract.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contracts.singular')]));

            return redirect(route('contracts.index'));
        }

        return view('contracts.show')->with('contract', $contract);
    }

    /**
     * Show the form for editing the specified Contract.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contracts.singular')]));

            return redirect(route('contracts.index'));
        }
        $this->data['contract'] = $contract;

        return view('contracts.edit')->with($this->data);
    }

    /**
     * Update the specified Contract in storage.
     *
     * @param  int              $id
     * @param UpdateContractRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContractRequest $request)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contracts.singular')]));

            return redirect(route('contracts.index'));
        }

        $contract = $this->contractRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/contracts.singular')]));

        return redirect(route('contracts.index'));
    }

    /**
     * Remove the specified Contract from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contract = $this->contractRepository->find($id);

        if (empty($contract)) {
            Flash::error(__('messages.not_found', ['model' => __('models/contracts.singular')]));

            return redirect(route('contracts.index'));
        }

        $this->contractRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/contracts.singular')]));

        return redirect(route('contracts.index'));
    }
}
