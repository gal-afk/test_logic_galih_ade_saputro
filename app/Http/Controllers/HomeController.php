<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'prima' => $this->bilanganPrima(100),
            'ganjil' => $this->bilanganGanjil(100),
            'genap' => $this->bilanganGenap(100),
            'fibonacci' => $this->deretFibonacci(100),
            'angkarandom' => $this->angkaRandom(),
            'angkakelipatantiga' => $this->angkaKelipatanTiga(),
        ];
        // dd($data);
        return view('home')->with($data);
    }


    public function bilanganPrima(int $max)
    {
        $arr = [];
        for ($i = 1; $i <= $max; $i++ ){
            $inc = 0;
            for ($b = 1; $b <= $max; $b++){

                if (fmod($i,$b) == 0){
                    $inc++;
                }
            }
            if ($inc == 2){
                $arr[] = $i;
                
            }
        }
            
        return $arr;
    }
    public function bilanganGanjil(int $max)
    {
        $arr = [];
        for ($i = 1; $i <= $max; $i++ ){
            if (fmod($i,2) != 0){
                $arr[]= $i;
            }
        }
            
        return $arr;
    }
    public function bilanganGenap(int $max)
    {
        $arr = [];
        for ($i = 1; $i <= $max; $i++ ){
            if (fmod($i,2) == 0){
                $arr[] = $i;
            }
        }
            
        return $arr;
    }
    public function deretFibonacci(int $max)
    {
        
        $a=0;
        $b=1;
        $arr = [$a,$b];
        for ($i = 0; $i <= $max; $i++ ){
            $sum = $a + $b;
            $arr[] = $sum;
            $a = $b;
            $b = $sum;
        }
            
        return $arr;
    }

    public function angkaRandom()
    {
        $arr = [];
        for ($i = 0; $i <= 5; $i++ ){
            $ran = $this->getRandomFive();
            if ($ran <= 60){
            $arr[] = $ran." Kurang";
            }
            if ($ran > 60 && $ran <= 70){
            $arr[] = $ran." Cukup";
            }
            if ($ran > 70 && $ran <= 80){
            $arr[] = $ran." Baik";
            }
            if ($ran > 80){
            $arr[] = $ran." Luar Biasa";
            }
        }
        return $arr;
    }

    public function getRandomFive()
    {
        $ran = rand(0,100);
        if(fmod($ran,5) == 0){
        return $ran;
        }else{
        return $this->getRandomFive();
        }
    }

    public function angkaKelipatanTiga()
    {
        $arr = [];
        for ($i = 1; $i <= 100; $i++ ){
            if(fmod($i,3) == 0){
                $arr[] = $i;
            }
        }
        return $arr;
    }

    public function filterArray(string $input)
    {
        $arr = array('Jakarta', 'Yogyakarta', 'Bandung', 'Bogor', 'Semarang');

        foreach ($arr as $val){
            if($val == $input){
            return true;
            }else{
            return false;
            }
        }
    }



}
