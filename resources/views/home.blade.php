@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Dashboard</h3>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h5>Bilangan Prima</h5>
                                    <div class="d-block">
                                    @foreach ($prima as $item)
                                        {{$item}},
                                    @endforeach
                                    </div>
                                    <br>
                                    
                                    <h5>Bilangan Ganjil</h5>
                                    <div class="d-block">
                                    @foreach ($ganjil as $item)
                                        {{$item}},
                                    @endforeach
                                    </div>
                                    <br>
                                    
                                    <h5>Bilangan Genap</h5>
                                    <div class="d-block">
                                    @foreach ($genap as $item)
                                        {{$item}},
                                    @endforeach
                                    </div>
                                    <br>
                                    <h5>Deret Fibonacci</h5>
                                    <div class="d-block">
                                    @foreach ($fibonacci as $item)
                                        {{$item}},
                                    @endforeach
                                    </div>
                                    <h5>Segitiga siku-siku</h5>
                                    <div class="d-block">
                                    @for ($i = 1; $i <= 5; $i++)
                                        @for($a = 1; $a <= $i; $a++)
                                            *
                                        @endfor
                                        <br>
                                    @endfor
                                    </div>
                                    <br>
                                    <h5>Angka random 1-100</h5>
                                    <div class="d-block">
                                    @foreach ($angkarandom as $item)
                                        {{$item}},
                                    @endforeach
                                    </div>
                                    <br>
                                    <h5>Angka Kelipatan Tiga</h5>
                                    <div class="d-block">
                                    @foreach ($angkakelipatantiga as $item)
                                        {{$item}},
                                    @endforeach
                                    </div>
                                    <br>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

