<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/employees.fields.name').':') !!}
    <p>{{ $employee->name }}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', __('models/employees.fields.nik').':') !!}
    <p>{{ $employee->nik }}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', __('models/employees.fields.alamat').':') !!}
    <p>{{ $employee->alamat }}</p>
</div>

<!-- Jen Kel Field -->
<div class="form-group">
    {!! Form::label('jen_kel', __('models/employees.fields.jen_kel').':') !!}
    <p>{{ $employee->jen_kel }}</p>
</div>

