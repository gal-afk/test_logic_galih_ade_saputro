@extends('layouts.app')
@section('title')
     @lang('models/contracts.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/contracts.plural')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('contracts.create')}}" class="btn btn-primary form-btn">Create <i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('contracts.table')
            </div>
       </div>
   </div>
    
    </section>
@endsection



