<!-- Employee Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('employee_id', __('models/contracts.fields.employee_id').':') !!}
    {!! Form::select('employee_id', $employee_list,null, ['class' => 'form-control select2']) !!}
</div>

<!-- Position Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('position_id', __('models/contracts.fields.position_id').':') !!}
    {!! Form::select('position_id',$position_list ,null, ['class' => 'form-control select2']) !!}
</div>

<!-- Date Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_start', __('models/contracts.fields.date_start').':') !!}
    {!! Form::date('date_start', null, ['class' => 'form-control','id'=>'date_start']) !!}
</div>


<!-- Date End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_end', __('models/contracts.fields.date_end').':') !!}
    {!! Form::date('date_end', null, ['class' => 'form-control','id'=>'date_end']) !!}
</div>


{{--<!-- Submit Field -->--}}
{{--<div class="form-group col-sm-12">--}}
{{--    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}--}}
{{--    <a href="{{ route('contracts.index') }}" class="btn btn-light">Cancel</a>--}}
{{--</div>--}}

@section('script')
    <script>

        $(document).ready(function() {
            $('.select2').select2();
            $('#date_end').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: false
            })
            $('#date_start').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                useCurrent: false
            })

        });

        $('.btn-save').on('click', function(e) {

            $.ajax({
                type: "POST",
                url: "{{ route('contracts.store') }}",
                enctype: 'multipart/form-data',
                data: new FormData(document.getElementById("form")),
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response) {
                    if (response.status == 'success') {
                        alert('Kontrak berhasil disimpan');
                        window.location.href = "{{ route('contracts.index') }}";
                    } else {
                        alert('Semething went wrong');
                    }
                },
                error: function(error) {}
            });
        });

    </script>

@endsection
