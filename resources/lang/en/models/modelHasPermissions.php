<?php

return array (
  'singular' => 'ModelHasPermission',
  'plural' => 'ModelHasPermissions',
  'fields' => 
  array (
    'permission_id' => 'Permission Id',
    'model_type' => 'Model Type',
    'model_id' => 'Model Id',
  ),
);
