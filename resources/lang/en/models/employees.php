<?php

return array (
  'singular' => 'Pegawai',
  'plural' => 'Pegawai',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'nik' => 'Nik',
    'alamat' => 'Alamat',
    'jen_kel' => 'Jenis Kelamin',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
  ),
);
