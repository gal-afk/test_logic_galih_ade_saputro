<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'Auth\AuthController@register');
    Route::post('login', 'Auth\AuthController@login' );
});

Route::get('get-roles', 'AppController@get_roles');

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('user', 'MemberController@show');
    Route::get('logout', 'Auth\AuthController@logout' );

});

